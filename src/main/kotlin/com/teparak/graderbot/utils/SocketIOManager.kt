package com.teparak.graderbot.utils

import com.corundumstudio.socketio.BroadcastOperations
import com.corundumstudio.socketio.SocketConfig
import com.corundumstudio.socketio.SocketIOServer
import com.teparak.graderbot.assignment.controller.submission.result.Message
import com.teparak.graderbot.assignment.controller.submission.result.ResultService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import com.corundumstudio.socketio.Configuration as SocketIOConfiguration


@Component
class SocketIOManager(val resultService: ResultService) {

    companion object {
        private val logger = LoggerFactory.getLogger(SocketIOManager::class.java)
        private val PORT = 8181
    }

    private var server: SocketIOServer? = null

    @PostConstruct
    fun init() {
        logger.debug("Initializing SocketIO server")

        val config = SocketIOConfiguration().apply {
            this.port = PORT
            this.workerThreads

            val socketConfig = SocketConfig().apply {
                this.isReuseAddress = true
            }

            this.socketConfig = socketConfig
        }

        server = SocketIOServer(config).apply {

            this.addConnectListener { client ->

                client.sendEvent("message", "${client.sessionId} joined")
                logger.debug("${client.sessionId} connected")
            }

            this.addEventListener("join", String::class.java) { client, submissionId, ackSender ->

                if (submissionId.isNullOrEmpty()) {
                    logger.debug("submission id is null or empty '$submissionId'")
                    client.disconnect()
                }

                client.joinRoom(submissionId)

                logger.debug("${client.sessionId} joined room: $submissionId")
                resultService.fetchLog(submissionId).forEach {
                    client.sendEvent("result", it)
                }

            }

            this.addEventListener("leave", String::class.java) { client, submissionId, ackSender ->
                if (submissionId.isNullOrEmpty()) {
                    logger.debug("submission id is null or empty '$submissionId'")
                    client.disconnect()
                }
                client.leaveRoom(submissionId)
            }

            this.addEventListener("disconnect", String::class.java) { client, data, ackSender ->
                val clientUid = client.sessionId
                client.disconnect()
                logger.debug("SessionID $clientUid disconnected!")
            }
        }

        server?.startAsync()

        logger.debug("SocketIO Server started")
    }

    fun sendMessage(room: String, message: Message) {
        val broadcast: BroadcastOperations = server?.getRoomOperations(room)!!
        broadcast.sendEvent("result", message)
    }

    fun sendEvent(room: String, message: Message) {
        val broadcast: BroadcastOperations = server?.getRoomOperations(room)!!
        broadcast.sendEvent("status", message)
    }

    @PreDestroy
    fun destroy() {
        logger.debug("Shutting down SocketIOServer")
        server?.stop()
        logger.debug("Shutdown SocketIOServer")
    }

}

data class JoinRoom(val room: String)