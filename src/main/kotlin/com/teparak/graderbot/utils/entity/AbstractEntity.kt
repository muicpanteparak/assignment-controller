package com.teparak.graderbot.utils.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.GenericGenerator
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.time.Instant
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractEntity : Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(updatable = false, nullable = false)
    @JsonIgnore
    var id: Long? = null

    @CreatedDate
    @Column(nullable = false, updatable = false)
    lateinit var created: Instant

    @LastModifiedDate
    @Column(nullable = false)
    lateinit var modified: Instant

    @Column(nullable = false)
    @ColumnDefault("false")
    var hidden: Boolean = false

    @Column(nullable = false)
    @ColumnDefault("false")
    var locked: Boolean = false

    @Column(nullable = false)
    @ColumnDefault("false")
    var deleted: Boolean = false
}