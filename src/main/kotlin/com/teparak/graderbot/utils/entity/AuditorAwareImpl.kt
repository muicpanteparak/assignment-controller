package com.teparak.graderbot.utils.entity

import org.springframework.data.domain.AuditorAware
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import java.util.*

@Component
class AuditorAwareImpl : AuditorAware<String> {
    override fun getCurrentAuditor(): Optional<String> {
        val context = SecurityContextHolder.getContext()
        return if (context.authentication != null) {
            Optional.ofNullable(context.authentication.principal as String)
        } else {
            Optional.ofNullable(null)
        }
    }
}