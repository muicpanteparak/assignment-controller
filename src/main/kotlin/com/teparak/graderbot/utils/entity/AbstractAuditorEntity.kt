package com.teparak.graderbot.utils.entity

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.LastModifiedBy
import javax.persistence.Column
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class AbstractAuditorEntity : AbstractEntity() {

    @CreatedBy
    @Column(nullable = false)
    var createdBy: String? = null

    @LastModifiedBy
    @Column(nullable = false)
    var modifiedBy: String? = null
}