package com.teparak.graderbot.assignment.controller.submission

import org.springframework.amqp.core.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class TaskQueueConfiguration {

    companion object {
        const val TASK_QUEUE = "task.queue.executor"
        const val TASK_EXCHANGE = "task.exchange"
    }

//    @Autowired
//    private lateinit var objectMapper: ObjectMapper
//
//    @Bean
//    fun converter(): MessageConverter {
//        return Jackson2JsonMessageConverter(objectMapper)
//    }

    @Bean
    fun taskQueueExchange(): Exchange {
        return ExchangeBuilder.topicExchange(TASK_EXCHANGE).durable(true).build()
    }

    @Bean
    fun binding(taskQueueBuilder: Queue, taskQueueExchange: Exchange): Binding {
        return BindingBuilder
                .bind(taskQueueBuilder)
                .to(taskQueueExchange)
                .with(taskQueueBuilder.name)
                .noargs()
    }

    @Bean
    fun taskQueueBuilder(): Queue {
        return QueueBuilder
                .durable(TASK_QUEUE)
                .build()
    }

//    @Bean
//    fun container(connectionFactory: CachingConnectionFactory, converter: MessageConverter): SimpleMessageListenerContainer {
//        val container = SimpleMessageListenerContainer()
//        container.connectionFactory = connectionFactory
//        return container
//    }
}