package com.teparak.graderbot.assignment.controller.user

import org.springframework.security.core.GrantedAuthority

enum class UserRole : GrantedAuthority {
    ROLE_ADMIN, ROLE_USER, ROLE_INCOMPLETE;

    override fun getAuthority(): String {
        return this.name
    }
}