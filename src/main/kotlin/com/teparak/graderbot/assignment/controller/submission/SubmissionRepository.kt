package com.teparak.graderbot.assignment.controller.submission

import com.teparak.graderbot.assignment.controller.management.task.Task
import org.springframework.data.jpa.repository.JpaRepository

interface SubmissionRepository : JpaRepository<Submission, Long> {
    fun findByUuid(uuid: String): Submission?
    fun findByCreatedByAndTask(createdBy: String, task: Task): MutableList<Submission>
}