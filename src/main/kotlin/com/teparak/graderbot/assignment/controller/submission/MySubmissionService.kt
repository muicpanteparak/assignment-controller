package com.teparak.graderbot.assignment.controller.submission

import com.teparak.graderbot.assignment.controller.management.task.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MySubmissionService {

    @Autowired
    private lateinit var submissionRepository: SubmissionRepository

    @Autowired
    private lateinit var taskService: TaskService

    fun mySubmission(uid: String, taskUid: String){
        var task = taskService.getTask(taskUid)
        println(submissionRepository.findByCreatedByAndTask(uid, task))
    }
}