package com.teparak.graderbot.assignment.controller.user

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserManagementService {

    companion object {
        private val logger = LoggerFactory.getLogger(UserManagementService::class.java)
    }

    @Autowired
    private lateinit var userAccountRepository: UserAccountRepository

    fun createOrGetUserAccount(uid: String): UserAccount {
        val user: UserAccount? = userAccountRepository.findByFirebaseUid(uid)

//        logger.debug("Upserting Firebase UserAccount")

        return if (user == null) {
            logger.debug("Firebase User Not Found, Creating...")
            val userAccount = UserAccount(
                    firebaseUid = uid,
                    userRole = UserRole.ROLE_USER
            )
            userAccountRepository.save(userAccount)
        } else {
//            logger.debug("Firebase User Found, updating first name and last name")
            user
        }
    }
}