package com.teparak.graderbot.assignment.controller.security.firebase

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class FirebaseAuthenticationToken : UsernamePasswordAuthenticationToken {
    constructor(token: String) : super(null, token)
    constructor(uid: String, authority: MutableList<out GrantedAuthority> = mutableListOf()) : super(uid, null, authority)
}