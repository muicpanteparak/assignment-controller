package com.teparak.graderbot.assignment.controller.submission

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class SubmissionStatusService {
    
    @Autowired
    private lateinit var submissionService: SubmissionService


    @Transactional
    fun updateStatus(submissionId: String, message: String, code: Int): Submission {
        return submissionService.findSubmissionByUid(submissionId)
                .apply {
                    this.currentStatus = message
                    this.running = arrayOf("kill", "die", "KILL", "DIE").contains(message)

                    if (message.contains("die", true)) {
                        this.exitCode = code
                    }
                }
    }

}