package com.teparak.graderbot.assignment.controller.submission.result

import com.teparak.graderbot.assignment.controller.submission.Submission
import org.springframework.data.jpa.repository.JpaRepository

interface ResultRepository : JpaRepository<Result, Long> {
    fun findBySubmissionOrderByColumnOrderAsc(submission: Submission): List<Result>
}