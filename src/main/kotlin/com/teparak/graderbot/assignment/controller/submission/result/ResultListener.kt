package com.teparak.graderbot.assignment.controller.submission.result

import com.teparak.graderbot.assignment.controller.submission.SubmissionStatusService
import com.teparak.graderbot.utils.SocketIOManager
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service

@Service
class ResultListener(
        val resultService: ResultService,
        val socketManager: SocketIOManager,
        val submissionStatus: SubmissionStatusService
) {

    companion object {
        private val logger = LoggerFactory.getLogger(ResultListener::class.java)
    }

    @RabbitListener(queues = [ResultQueueConfiguration.QUEUE])
    fun listener(payload: Message) {
        logger.debug("Receiving Message - Recipient: ${payload.submissionUid} Message Order: ${payload.order}")
        logger.debug(payload.message)

        if (payload.type == MessageType.MESSAGE) {
            resultService.log(message = payload)
            socketManager.sendMessage(payload.submissionUid, payload)
        } else if (payload.type == MessageType.EVENT) {
            submissionStatus.updateStatus(
                    submissionId = payload.submissionUid,
                    message = payload.message,
                    code = payload.order
            )
            socketManager.sendEvent(payload.submissionUid, payload)
        }

    }
}

enum class MessageType {
    MESSAGE, EVENT
}


sealed class BaseMessage(open var type: MessageType = MessageType.MESSAGE, open val submissionUid: String)
data class Message(val message: String, val order: Int = 0, override val submissionUid: String, override var type: MessageType) : BaseMessage(type, submissionUid)