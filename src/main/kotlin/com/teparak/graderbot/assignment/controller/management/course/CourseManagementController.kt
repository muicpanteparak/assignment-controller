package com.teparak.graderbot.assignment.controller.management.course

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/course")
class CourseManagementController {

    @Autowired
    private lateinit var courseManagementService: CourseManagementService

    @PostMapping
    fun createCourse(){
        courseManagementService.createCourse()
    }

    @GetMapping
    fun getMyCourse(){
        courseManagementService.getCourse()
    }

    @PatchMapping
    fun editCourse() {
        courseManagementService.editCourse()
    }
}