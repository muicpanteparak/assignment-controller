package com.teparak.graderbot.assignment.controller.submission

import com.teparak.graderbot.assignment.controller.upload.UploadService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/management/subm")
class SubmissionController {

    companion object {
        private val logger = LoggerFactory.getLogger(SubmissionController::class.java)
    }

    @Autowired
    private lateinit var uploadService: UploadService

    @Autowired
    private lateinit var submissionService: SubmissionService

//    @Autowired
//    private lateinit var taskService: TaskService


    @Autowired
    private lateinit var mySubmissionService: MySubmissionService

    @RequestMapping("/{uid}", method = [RequestMethod.POST], params = ["length"])
    @ResponseStatus(HttpStatus.CREATED)
    fun uploadSubmission(@PathVariable uid: String, request: HttpServletRequest, @RequestParam(required = true) length: Long): SubmissionDTO? {
        try {
            uploadService.readFormData(request, length, SubmitAssignment::class) { data: SubmitAssignment, fileObjectDTO ->

                val submissionDTO = submissionService.submit(uid, data, fileObjectDTO)
                throw ResultException(submissionDTO)
            }
        } catch (result: ResultException) {
            return result.submission
        }
        return null
    }

    @GetMapping("/{uid}")
    fun getMySubmission(authentication: Authentication, @PathVariable uid: String){
        println(authentication.principal)
    }
}

class ResultException(val submission: SubmissionDTO) : RuntimeException()

