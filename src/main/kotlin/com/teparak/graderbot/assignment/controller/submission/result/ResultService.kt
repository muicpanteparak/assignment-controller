package com.teparak.graderbot.assignment.controller.submission.result

import com.teparak.graderbot.assignment.controller.submission.SubmissionService
import org.springframework.stereotype.Service

@Service
class ResultService(
        val resultRepository: ResultRepository,
        val submissionService: SubmissionService) {

    fun log(message: Message) {
        val submission = submissionService.findSubmissionByUid(message.submissionUid)

        val result = Result(
                submission = submission,
                output = message.message,
                columnOrder = message.order
        )

        resultRepository.save(result)
    }

    fun fetchLog(submissionUid: String): List<ResultDTO> {
        val subm = submissionService.findSubmissionByUid(submissionUid)
        return resultRepository.findBySubmissionOrderByColumnOrderAsc(subm)
                .map {
                    it.toResultDTO()
                }
    }

//    fun getLastEvent(submissionUid: String): {
//
//    }

}