package com.teparak.graderbot.assignment.controller.management.task

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/management/task")
class TaskController {

    @Autowired
    private lateinit var taskService: TaskService

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createTask(@RequestBody task: DetailedTaskDTO): TaskDTO {
        return taskService.createTask(task)
    }

    @PatchMapping("{uid}")
    fun editTask(@RequestBody editTask: EditTask, @PathVariable uid: String): TaskDTO {
        return taskService.editTask(uid, editTask)
    }

    @GetMapping("{uid}")
    fun getTask(@PathVariable uid: String): TaskDTO {
        return taskService.getTaskDTO(uid)
    }

    @GetMapping
    fun listTask(): ListTaskDTO {
        return taskService.listTask()
    }
}


