package com.teparak.graderbot.assignment.controller.submission.result

import com.teparak.graderbot.assignment.controller.submission.Submission
import com.teparak.graderbot.utils.entity.AbstractEntity
import javax.persistence.*

@Entity
class Result(

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "fk_submission")
        var submission: Submission,

        @Column(columnDefinition = "LONGTEXT")
        var output: String,

        @Column(nullable = false, updatable = false)
        var columnOrder: Int = 0

) : AbstractEntity()


data class ResultDTO(val output: String, val columnOrder: Int)

fun Result.toResultDTO(): ResultDTO {
        return ResultDTO(this.output, this.columnOrder)
}