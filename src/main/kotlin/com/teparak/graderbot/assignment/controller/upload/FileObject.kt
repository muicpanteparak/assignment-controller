package com.teparak.graderbot.assignment.controller.upload

import com.teparak.graderbot.assignment.controller.management.testcase.TestCase
import com.teparak.graderbot.assignment.controller.submission.Submission
import com.teparak.graderbot.utils.entity.AbstractAuditorEntity
import org.hibernate.annotations.ColumnDefault
import javax.persistence.*

@Entity
class FileObject(

        @Column(nullable = false)
        var contentType: String? = null,

        @Column(nullable = false)
        var bucket: String? = null,

        @Column(nullable = false)
        var objectKey: String? = null,

        @Column(nullable = false)
        var originalName: String? = null,

        @Column(nullable = false)
        @ColumnDefault("0")
        var size: Long = 0,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "fk_testcase")
        var testcase: TestCase? = null,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "fk_submission")
        var submission: Submission? = null

) : AbstractAuditorEntity()