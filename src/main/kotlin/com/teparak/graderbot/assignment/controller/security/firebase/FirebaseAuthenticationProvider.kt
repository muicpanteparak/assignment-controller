package com.teparak.graderbot.assignment.controller.security.firebase

import com.google.firebase.auth.FirebaseAuth
import com.teparak.graderbot.assignment.controller.user.UserManagementService
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component


@Component
class FirebaseAuthenticationProvider(
        val firebaseAuth: FirebaseAuth,
        val userManagementService: UserManagementService
) : AuthenticationProvider {

    companion object {
        private val logger = LoggerFactory.getLogger(FirebaseAuthenticationProvider::class.java)
    }

    override fun authenticate(authentication: Authentication): Authentication? {
        val token = authentication.credentials as String

        return try {
            firebaseAuth.verifyIdToken(token).run {
                val user = userManagementService.createOrGetUserAccount(uid)
                FirebaseAuthenticationToken(uid!!, mutableListOf(user.userRole))
            }

        } catch (e: Exception) {
            logger.error("Firebase Authentication Exception")
            null
        }
    }

    override fun supports(clazz: Class<*>): Boolean = FirebaseAuthenticationToken::class.java.isAssignableFrom(clazz)

}