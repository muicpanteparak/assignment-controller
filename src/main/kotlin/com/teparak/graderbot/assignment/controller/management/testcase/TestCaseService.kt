package com.teparak.graderbot.assignment.controller.management.testcase

import com.teparak.graderbot.assignment.controller.management.task.TaskService
import com.teparak.graderbot.assignment.controller.upload.FileObject
import com.teparak.graderbot.assignment.controller.upload.FileObjectDTO
import com.teparak.graderbot.assignment.controller.upload.FileObjectRepository
import com.teparak.graderbot.storage.StorageService
import org.hibernate.Hibernate
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TestCaseService(
        val storageService: StorageService,
        val taskService: TaskService,
        val testCaseRepository: TestCaseRepository,
        val fileObjectRepository: FileObjectRepository
) {

    @Transactional
    fun addFile(id: String, metadata: TestCaseData, fileDTO: FileObjectDTO, preprocess: ((String, String) -> Unit)? = null): TestCaseDTO {
        val task = taskService.findByUuidNotLocked(id)
        val (name, point, testType) = metadata

        val (bucket, key) = fileDTO

        val testObjectKey = "task/$key"
        storageService.moveObject(bucket, key, "${task.uuid}", testObjectKey)

        var testCase = TestCase(
                testCaseName = name,
                score = point,
                testCaseType = testType,
                task = task
        )

        testCase = testCaseRepository.save(testCase)

        Hibernate.initialize(task.testcases)
        task.testcases.add(testCase)

        var file = FileObject(
                bucket = task.uuid,
                objectKey = testObjectKey,
                contentType = fileDTO.metadata.contentType,
                size = fileDTO.metadata.contentLength,
                originalName = fileDTO.originalFileName,
                testcase = testCase
        )

        file = fileObjectRepository.save(file)
        file.testcase = testCase

        task.hidden = false

        return testCase.toDTO()
    }

    fun listTest(uuid: String): List<TestCaseDTO> {
        return getTest(uuid).map {
            TestCaseDTO(
                    name = it.testCaseName!!,
                    score = it.score,
                    created = it.created,
                    modified = it.modified,
                    testCaseType = it.testCaseType
            )
        }
    }

    fun editTest(uid: String){
        throw NotImplementedError("Method not implemented")
    }

    private fun getTest(uuid: String): List<TestCase> {
        return testCaseRepository.findByTask(taskService.findByUuidNotLocked(uuid))
    }
}


data class TestCaseData(val name: String, val point: Int = 1, val testCaseType: TestCaseType = TestCaseType.PRIVATE)