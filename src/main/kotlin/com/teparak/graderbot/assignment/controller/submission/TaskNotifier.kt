package com.teparak.graderbot.assignment.controller.submission

import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class TaskNotifier {

    @Autowired
    private lateinit var notify: RabbitTemplate

    fun <T : RabbitMessage> notifyMaster(message: T) {
        notify.convertAndSend(TaskQueueConfiguration.TASK_EXCHANGE, TaskQueueConfiguration.TASK_QUEUE, message)
    }
}