package com.teparak.graderbot.assignment.controller

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.teparak.graderbot")
@EntityScan("com.teparak.graderbot")
class AssignmentControllerApplication

fun main(args: Array<String>) {
    runApplication<AssignmentControllerApplication>(*args)
}
