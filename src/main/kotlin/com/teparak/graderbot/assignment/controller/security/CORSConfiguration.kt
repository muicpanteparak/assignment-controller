package com.teparak.graderbot.assignment.controller.security

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource


@Configuration
class CORSConfiguration {

    companion object {
        private val logger = LoggerFactory.getLogger(CORSConfiguration::class.java)
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource { // DO NOT CHANGE METHOD NAME
        logger.debug("Configuring CorsConfiguration")
        val configuration = CorsConfiguration()
                .apply {
                    this.allowedOrigins = listOf("http://localhost:8085")
                    this.allowedMethods = listOf("GET", "POST", "PUT", "PATCH", "DELETE")
                    this.allowedHeaders = listOf("Content-Type", "X-Firebase-Auth", "Accept")
                    this.allowCredentials = true
                    this.maxAge = 60 * 60
                }

        return UrlBasedCorsConfigurationSource()
                .apply {
                    this.registerCorsConfiguration("/**", configuration)
                }
    }
}

