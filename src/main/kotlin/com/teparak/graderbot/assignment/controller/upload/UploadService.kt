package com.teparak.graderbot.assignment.controller.upload

import com.amazonaws.services.s3.model.ObjectMetadata
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.ObjectMapper
import com.teparak.graderbot.exception.BadRequestException
import com.teparak.graderbot.storage.StorageService
import com.teparak.graderbot.utils.RandomFactory
import org.apache.commons.io.IOUtils
import org.apache.http.entity.ContentType
import org.apache.tika.Tika
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.BufferedInputStream
import java.io.InputStream
import java.nio.charset.Charset
import javax.servlet.http.HttpServletRequest
import kotlin.reflect.KClass

@Service
class UploadService {

    companion object {
        private var logger = LoggerFactory.getLogger(UploadService::class.java)
    }

    @Autowired
    private lateinit var storageService: StorageService

    @Autowired
    private lateinit var randomFactory: RandomFactory

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    fun <T : Any> readFormData(request: HttpServletRequest, fileLength: Long?, clazz: KClass<T>, onComplete: (((T, FileObjectDTO) -> Unit))? = null) {
        val TEMP_BUCKET = "temp"

        val upload  = ServletFileUpload()
        val iter = upload.getItemIterator(request)
        val isMultipart = ServletFileUpload.isMultipartContent(request)

        if (!isMultipart && fileLength == null) throw BadRequestException()

        val map: MutableMap<String, String> = mutableMapOf()
        val metas = mutableListOf<FileObjectDTO>()

        while (iter.hasNext()){
            val item = iter.next()
            val name = item.fieldName

            val stream = item.openStream()

            if (!item.isFormField){
                val tempKey = randomFactory.alphanumericalIgnoreCase(10, 32)
                val originalFileName = item.name
                val content = FileMetadata(contentType = item.contentType, inputStream = stream, filename = originalFileName, size = fileLength!!)
                logger.debug("Uploading content")
                val meta = this.store(fileContent = content, bucket = TEMP_BUCKET, key = tempKey)

                metas.add(FileObjectDTO(TEMP_BUCKET, tempKey, meta, originalFileName))
            } else {
                val payload = IOUtils.toString(stream, Charset.defaultCharset())
                map[name] = payload
                logger.debug("Found '$name' -> '$payload'")
            }
        }

        val data: T = objectMapper.convertValue(map, clazz.java)

        if (metas.size == 0) throw BadRequestException("No File in Form Data")
        onComplete?.invoke(data, metas[0])
    }

    private fun store(fileContent: FileMetadata, bucket: String, key: String): ObjectMetadata {
        storageService.createBucket(bucket)
        val (_: String, size: Long, inputStream: InputStream, originalFilename) = fileContent

        val getContentType: (InputStream) -> String = { inputs: InputStream ->
            Tika().detect(inputs, originalFilename) ?: ContentType.APPLICATION_OCTET_STREAM.mimeType
        }

        val stream: BufferedInputStream = when(inputStream !is BufferedInputStream){
            true -> BufferedInputStream(inputStream, 3 * 1024 * 1024)
            else -> inputStream as BufferedInputStream
        }

        val predictedContentType = getContentType(stream)
        logger.debug("Predicted content-type '$predictedContentType'")

        val meta: ObjectMetadata = ObjectMetadata().apply {
            this.contentType = predictedContentType
            this.contentLength = size
        }

        storageService.putObject(bucket = bucket, objectName = key, inputStream = stream,  metadata = meta)
        return meta
    }
}

data class FileMetadata(val contentType: String, val size: Long, @JsonIgnore val inputStream: InputStream, val filename: String)
data class FileObjectDTO(val bucket: String, val objectKey: String, val metadata: ObjectMetadata, val originalFileName: String)