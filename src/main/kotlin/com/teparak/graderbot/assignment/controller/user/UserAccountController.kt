package com.teparak.graderbot.assignment.controller.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/user")
class UserAccountController {

    @Autowired
    private lateinit var userManagementService: UserManagementService


    @GetMapping("/role")
//    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    fun userRole(authentication: Authentication): UserPermissionDTO {
        return userManagementService.createOrGetUserAccount(authentication.principal as String).toPermissionDTO()
    }
}