package com.teparak.graderbot.assignment.controller.management.task

enum class TaskType {
    ASSIGNMENT, PRACTICE
}