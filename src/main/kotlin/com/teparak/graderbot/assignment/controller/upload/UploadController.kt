package com.teparak.graderbot.assignment.controller.upload

import com.fasterxml.jackson.databind.ObjectMapper
import com.teparak.graderbot.assignment.controller.management.testcase.TestCaseService
import com.teparak.graderbot.assignment.controller.submission.SubmissionService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/management/upload")
@RestController
class UploadController {

    companion object {
        private val logger = LoggerFactory.getLogger(UploadController::class.java)
    }

    @Autowired
    private lateinit var testCaseService: TestCaseService

    @Autowired
    private lateinit var uploadService: UploadService

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var submissionService: SubmissionService

}