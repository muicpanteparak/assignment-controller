package com.teparak.graderbot.assignment.controller.management.task

import org.springframework.data.jpa.repository.JpaRepository

interface TaskRepository : JpaRepository<Task, Long> {
    fun findByUuidAndLockedAndHidden(uuid: String, locked: Boolean, hidden: Boolean): Task?
    fun findByUuidAndLocked(uuid: String, locked: Boolean): Task?
    fun findByUuid(uuid: String): Task?
    fun findByLockedAndHidden(locked: Boolean, hidden: Boolean): List<Task>
}
