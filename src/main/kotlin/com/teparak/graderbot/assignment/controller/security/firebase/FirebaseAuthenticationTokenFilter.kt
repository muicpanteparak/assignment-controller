package com.teparak.graderbot.assignment.controller.security.firebase

import org.springframework.http.HttpMethod
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class FirebaseAuthenticationTokenFilter : AbstractAuthenticationProcessingFilter("/**") {
    companion object {
        private const val TOKEN_HEADER = "X-Firebase-Auth"
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication? {
        val authToken = request.getHeader(TOKEN_HEADER)

        if (HttpMethod.OPTIONS.matches(request.method)) {
            logger.debug("Cross-Origin Resource Sharing Preflight Request")
            return null
        }

        if (authToken.isNullOrBlank()) {
            logger.error("header: '$TOKEN_HEADER' not supplied")
            return null
        }

        return authenticationManager.authenticate(FirebaseAuthenticationToken(authToken))
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        super.successfulAuthentication(request, response, chain, authResult)
        chain.doFilter(request, response)
    }

    override fun unsuccessfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, failed: AuthenticationException?) {
        response.status = 401
    }
}