package com.teparak.graderbot.assignment.controller.management.task

enum class TaskDifficulty {
    EASY, MEDIUM, HARD
}