package com.teparak.graderbot.assignment.controller.user

import com.teparak.graderbot.utils.entity.AbstractEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated

@Entity
class UserAccount(

        @Column(nullable = false, updatable = false, unique = true)
        var firebaseUid: String,

        @Column(nullable = false)
        @Enumerated(EnumType.STRING)
        var userRole: UserRole
) : AbstractEntity()


data class UserPermissionDTO(val uid: String, val role: UserRole)

fun UserAccount.toPermissionDTO() = UserPermissionDTO(
        uid = this.firebaseUid,
        role = this.userRole
)