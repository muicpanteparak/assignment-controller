package com.teparak.graderbot.assignment.controller.submission.result

import org.springframework.amqp.core.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class ResultQueueConfiguration {

    companion object {
        const val QUEUE = "result_output.queue.executor"
        const val EXCHANGE = "result_output.exchange"
    }

    @Bean
    fun resultQueueExchange(): Exchange {
        return ExchangeBuilder.topicExchange(EXCHANGE).durable(true).build()
    }


    @Bean
    fun resultQueueBinding(resultQueueBuilder: Queue, resultQueueExchange: Exchange): Binding {
        return BindingBuilder
                .bind(resultQueueBuilder)
                .to(resultQueueExchange)
                .with(resultQueueBuilder.name)
                .noargs()
    }

    @Bean
    fun resultQueueBuilder(): Queue {
        return QueueBuilder
                .durable(QUEUE)
                .build()
    }
}