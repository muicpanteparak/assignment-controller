package com.teparak.graderbot.assignment.controller.submission

import com.teparak.graderbot.assignment.controller.management.task.Task
import com.teparak.graderbot.assignment.controller.submission.result.Result
import com.teparak.graderbot.assignment.controller.upload.FileObject
import com.teparak.graderbot.utils.entity.AbstractAuditorEntity
import javax.persistence.*

@Entity
class Submission(

        @Column(nullable = false, updatable = false, unique = true)
        var uuid: String,

        @Column(nullable = true)
        var lang: String? = null,

        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "submission")
        var files: MutableList<FileObject> = mutableListOf(),

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "fk_task")
        var task: Task? = null,

        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "submission")
        var filteredLog: List<Result> = mutableListOf(),


        var exitCode: Int = -1,

        var running: Boolean = false,

        @Column(nullable = true)
        var currentStatus: String? = null

) : AbstractAuditorEntity()

data class SubmissionDTO(
        val uuid: String,
        val language: String
)

fun Submission.toDTO(): SubmissionDTO = SubmissionDTO(
        uuid = this.uuid,
        language = this.lang ?: ""
)