package com.teparak.graderbot.assignment.controller.security.firebase

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File
import java.io.FileInputStream


@Configuration
@ConfigurationProperties("com.google.firebase")
class FirebaseAuthenticationConfiguration {

    companion object {
        private val logger = LoggerFactory.getLogger(FirebaseAuthenticationConfiguration::class.java)
    }

    var serviceAccountFile: String? = null

    @Bean
    fun firebaseAuth(): FirebaseAuth {

        val f = javaClass.classLoader.getResource(serviceAccountFile)

        if (serviceAccountFile.isNullOrBlank() || f == null) {
            logger.warn("Firebase Service Account File Not Found")
            throw Exception("Firebase Service Account File Not Found")
        }

        val file = File(f.file)

        if (!file.isFile) {
            logger.warn("Firebase Service Account File Not Found")
            throw Exception("Firebase Service Account File Not Found")
        }

        val options = FirebaseOptions
                .Builder()
                .setCredentials(GoogleCredentials.fromStream(FileInputStream(file)))
                .build()

        logger.debug("Initializing Firebase")
        FirebaseApp.initializeApp(options)
        return FirebaseAuth.getInstance()
    }
}