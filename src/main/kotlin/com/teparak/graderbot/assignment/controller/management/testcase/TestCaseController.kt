package com.teparak.graderbot.assignment.controller.management.testcase

import com.teparak.graderbot.assignment.controller.upload.FileObjectDTO
import com.teparak.graderbot.assignment.controller.upload.UploadService
import com.teparak.graderbot.exception.BadRequestException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest


@RestController
@RequestMapping("/management/testcase")
class TestCaseController(
        val uploadService: UploadService,
        val testCaseService: TestCaseService) {

    companion object {
        private var logger = LoggerFactory.getLogger(TestCaseController::class.java)
    }

    @GetMapping("/{uid}")
    fun listTests(@PathVariable uid: String) {

    }

    @RequestMapping("/{uid}", method = [RequestMethod.POST], params = ["length"])
    @ResponseStatus(HttpStatus.CREATED)
    fun uploadTestcase(@PathVariable uid: String, request: HttpServletRequest, @RequestParam(required = true) length: Long) {
        logger.debug("Path: ${request.contextPath}/${request.servletPath}")

        uploadService.readFormData(request, length, TestCaseData::class) { testCaseData: TestCaseData, fileObjectDTO: FileObjectDTO ->
            try {
                testCaseService.addFile(uid, testCaseData, fileObjectDTO)
            } catch (ex: IllegalArgumentException) {
                logger.warn(ex.message)
                throw BadRequestException(ex.message)
            }
        }
    }
}
