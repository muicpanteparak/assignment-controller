package com.teparak.graderbot.assignment.controller.upload

import org.springframework.data.jpa.repository.JpaRepository

interface FileObjectRepository : JpaRepository<FileObject, Long>