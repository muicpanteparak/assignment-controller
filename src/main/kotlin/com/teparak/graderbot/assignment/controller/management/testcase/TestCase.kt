package com.teparak.graderbot.assignment.controller.management.testcase

import com.teparak.graderbot.assignment.controller.management.task.Task
import com.teparak.graderbot.assignment.controller.upload.FileObject
import com.teparak.graderbot.utils.entity.AbstractAuditorEntity
import java.time.Instant
import javax.persistence.*

@Entity
class TestCase(

        @Column(nullable = false, length = 100)
        var testCaseName: String? = null,

        @Column(nullable = false)
        var score: Int = 0,

        @Column(nullable = false)
        @Enumerated(EnumType.STRING)
        var testCaseType: TestCaseType = TestCaseType.PRIVATE,

        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "testcase")
        var testfile: MutableList<FileObject> = mutableListOf(),

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "fk_task")
        var task: Task? = null

) : AbstractAuditorEntity()

data class TestCaseDTO(val name: String, val score: Int = 0, val created: Instant, val modified: Instant, val testCaseType: TestCaseType)

fun TestCase.toDTO(): TestCaseDTO {
        return TestCaseDTO(
                name = this.testCaseName ?: "",
                score = this.score,
                testCaseType = this.testCaseType,
                created = this.created,
                modified = this.modified
        )
}
