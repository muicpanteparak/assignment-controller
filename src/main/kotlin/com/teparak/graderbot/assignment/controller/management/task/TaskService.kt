package com.teparak.graderbot.assignment.controller.management.task

import com.teparak.graderbot.exception.RecordNotFoundException
import com.teparak.graderbot.utils.RandomFactory
import org.modelmapper.ModelMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TaskService {

    companion object {
        private val logger = LoggerFactory.getLogger(TaskService::class.java)
    }

    @Autowired
    private lateinit var taskRepository: TaskRepository

    @Autowired
    private lateinit var modelMapper: ModelMapper

    @Autowired
    private lateinit var randomFactory: RandomFactory

    fun createTask(taskSchema: DetailedTaskDTO): TaskDTO {

        logger.debug("creating new task")
        val (name, _, _, difficulty, starterCode, _, taskType, details, submissionName) = taskSchema

        val task = Task(
                taskName = name,
                taskType = taskType,
                uuid = randomFactory.alphanumericalIgnoreCase(6, 20),
                score = 0,
                difficulty = difficulty,
                starterCode = starterCode,
                details = details,
                submissionName = submissionName ?: ""
        )
                .apply { this.hidden = true }

        return taskRepository
                .save(task)
                .toDTO()
    }


    fun getTaskDTO(uid: String): DetailedTaskDTO {
        val task = findByUuidNotHidden(uid)
        return task
                .toDetailedDTO()
    }

    fun getTask(uid: String): Task {
        val task = findByUuidNotHidden(uid)
        return task
    }

    @Throws(RecordNotFoundException::class)
    fun editTask(id: String, editTask: EditTask): DetailedTaskDTO {
        val task = findByUuidNotHidden(id)
        modelMapper.map(editTask, task)
        val savedTask = taskRepository.save(task)
        return savedTask.toDetailedDTO()
    }

    fun listTask(): ListTaskDTO {
        val data = findAllByUuidNotHidden()
        return ListTaskDTO().apply {
            this.tasks = data
                    .map {
                        it.toDTO()
                    }
        }
    }

    fun disableTask(uuid: String) {
        val task = findByUuid(uuid)
        task.locked = true
    }

    private fun findByUuid(uuid: String): Task {
        return taskRepository.findByUuid(uuid) ?: throw RecordNotFoundException("UUID does not exist")
    }

    fun findByUuidNotLocked(uuid: String): Task {
        return taskRepository.findByUuidAndLocked(uuid = uuid, locked = false)
                ?: throw RecordNotFoundException("UUID does not exist")
    }

    private fun findAllByUuidNotHidden(): List<Task> {
        return taskRepository.findByLockedAndHidden(locked = false, hidden = false)
    }

    private fun findByUuidNotHidden(uuid: String): Task {
        return taskRepository.findByUuidAndLockedAndHidden(uuid = uuid, locked = false, hidden = false)
                ?: throw RecordNotFoundException("UUID does not exist")
    }
}

data class ListTaskDTO(var tasks: List<TaskDTO> = mutableListOf())


data class EditTask(
        var name: String? = null,
        var taskType: TaskType? = null,
        val version: Int)