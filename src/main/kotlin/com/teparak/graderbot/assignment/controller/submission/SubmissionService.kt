package com.teparak.graderbot.assignment.controller.submission

import com.teparak.graderbot.assignment.controller.management.task.TaskService
import com.teparak.graderbot.assignment.controller.upload.FileObject
import com.teparak.graderbot.assignment.controller.upload.FileObjectDTO
import com.teparak.graderbot.assignment.controller.upload.FileObjectRepository
import com.teparak.graderbot.exception.BadRequestException
import com.teparak.graderbot.exception.RecordNotFoundException
import com.teparak.graderbot.storage.StorageService
import com.teparak.graderbot.utils.RandomFactory
import org.hibernate.Hibernate
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class SubmissionService(
        private val storageService: StorageService,
        private val taskService: TaskService,
        private val fileObjectRepository: FileObjectRepository,
        private val randomFactory: RandomFactory,
        private val submissionRepository: SubmissionRepository,
        private val notifier: TaskNotifier) {

    @Transactional
    fun submit(taskId: String, submitAssignment: SubmitAssignment, fileObjectDTO: FileObjectDTO): SubmissionDTO {
        val (language) = submitAssignment
        val task = taskService.findByUuidNotLocked(taskId)

        val (
                srcBucket,
                srcKey,
                objectMetadata,
                _

        ) = fileObjectDTO

        val key = "subm/$srcKey"

        storageService.moveObject(srcBucket, srcKey, task.uuid!!, key)

        var file = FileObject(
                bucket = task.uuid,
                objectKey = key,
                contentType = objectMetadata.contentType,
                size = objectMetadata.contentLength,
                originalName = task.submissionName
        )

        file = fileObjectRepository.save(file)
        var subm = Submission(
                lang = language,
                files = mutableListOf(file),
                task = task,
                uuid = randomFactory.alphanumericalIgnoreCase(5, 20),
                exitCode = -1
        )

        file.submission = subm

        subm = submissionRepository.save(subm)

        Hibernate.initialize(task.submission)
        task.submission.add(subm)

        Hibernate.initialize(task.testcases)

        if (task.testcases.isEmpty()) {
            throw BadRequestException("No Testcase")
        }

        val submission = SubmissionMetadata(subm.files[0].bucket!!, subm.files[0].objectKey!!, subm.files[0].originalName!!)
        val payload = SubmitAssignmentMetadata(
                language = language,
                tests = task.testcases.map { TestCaseMetadata(it.testfile.last().bucket!!, it.testfile.last().objectKey!!, it.testfile.last().originalName!!) },
                files = submission,
                submissionUid = subm.uuid
        )

        notifier.notifyMaster(payload)

        return subm.toDTO()
    }

    fun findSubmissionByUid(submissionUid: String): Submission {
        return submissionRepository.findByUuid(
                uuid = submissionUid
        ) ?: throw RecordNotFoundException("Submission UID Not Found")
    }
}

data class SubmitAssignment(val language: String)
sealed class RabbitMessage
sealed class ObjectMeta(open val bucket: String, open val objectKey: String, open val originalName: String)
data class TestCaseMetadata(override val bucket: String, override val objectKey: String, override val originalName: String) : ObjectMeta(bucket, objectKey, originalName)
data class SubmissionMetadata(override val bucket: String, override val objectKey: String, override val originalName: String) : ObjectMeta(bucket, objectKey, originalName)
data class SubmitAssignmentMetadata(val language: String, val tests: List<TestCaseMetadata>, val files: SubmissionMetadata, val submissionUid: String) : RabbitMessage()


//data class TaskSubmissionMetadata(override val bucket: String, override val objectKey: String) : ObjectMeta(bucket = bucket, objectKey = objectKey)
//data class TestSubmissionMetadata(override val bucket: String, override val objectKey: String): ObjectMeta(bucket = bucket, objectKey = objectKey)
//data class SubmitAssignmentAssignmentMetadata(val lang: String, val submission: List<TaskSubmissionMetadata>, val test: List<TestSubmissionMetadata>) : RabbitMessage()