package com.teparak.graderbot.assignment.controller.management.testcase

enum class TestCaseType {
    PUBLIC, PRIVATE
}