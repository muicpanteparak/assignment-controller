package com.teparak.graderbot.assignment.controller.user

import org.springframework.data.jpa.repository.JpaRepository

interface UserAccountRepository : JpaRepository<UserAccount, Long> {
    fun findByFirebaseUid(firebaseUid: String): UserAccount?
}