package com.teparak.graderbot.assignment.controller.management.task

import com.teparak.graderbot.assignment.controller.management.testcase.TestCase
import com.teparak.graderbot.assignment.controller.submission.Submission
import com.teparak.graderbot.utils.entity.AbstractAuditorEntity
import org.hibernate.annotations.ColumnDefault
import javax.persistence.*


@Entity
class Task(
        @Column(nullable = false, updatable = false)
        var uuid: String? = null,

        @Column(nullable = false, length = 100)
        var taskName: String? = null,

        @Enumerated(EnumType.STRING)
        @Column(nullable = false, unique = false)
        var taskType: TaskType? = null,

        @Column(nullable = false, columnDefinition = "LONGTEXT")
        var details: String? = null,

        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "task")
        var testcases: MutableList<TestCase> = mutableListOf(),

        @Column(nullable = false)
        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "task")
        var submission: MutableList<Submission> = mutableListOf(),

        @Column(nullable = false)
        var submissionName: String,

        @ColumnDefault("0")
        @Column(nullable = false)
        var score: Int = 0,

        @ColumnDefault("'EASY'")
        @Enumerated(EnumType.STRING)
        @Column(nullable = false)
        var difficulty: TaskDifficulty = TaskDifficulty.EASY,

        @Column(nullable = false, columnDefinition = "LONGTEXT")
        var starterCode: String = "",

        @Version
        @ColumnDefault("0")
        var version: Int = 0
) : AbstractAuditorEntity()


open class TaskDTO(open val name: String, open val point: Int, open var uuid: String? = null, open val difficulty: TaskDifficulty)

data class DetailedTaskDTO(
        override val name: String,
        override val point: Int,
        override var uuid: String?,
        override val difficulty: TaskDifficulty,
        val starterCode: String,
        val version: Int = 0,
        val taskType: TaskType,
        val details: String,
        var submissionName: String
) : TaskDTO(
        name = name,
        point = point,
        uuid = uuid,
        difficulty = difficulty)

fun Task.toDetailedDTO(): DetailedTaskDTO {
        return DetailedTaskDTO(
                name = this.taskName!!,
                point = this.score,
                uuid = this.uuid!!,
                difficulty = this.difficulty,
                starterCode = this.starterCode,
                version = this.version,
                taskType = this.taskType!!,
                details = this.details ?: "",
                submissionName = submissionName
        )
}

fun Task.toDTO(): TaskDTO {
        return TaskDTO(
                name = this.taskName!!,
                point = this.score,
                uuid = this.uuid!!,
                difficulty = this.difficulty
        )
}