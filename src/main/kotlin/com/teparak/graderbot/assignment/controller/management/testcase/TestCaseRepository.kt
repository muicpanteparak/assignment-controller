package com.teparak.graderbot.assignment.controller.management.testcase

import com.teparak.graderbot.assignment.controller.management.task.Task
import org.springframework.data.jpa.repository.JpaRepository

interface TestCaseRepository : JpaRepository<TestCase, Long> {
    fun findByTask(task: Task): List<TestCase>
}