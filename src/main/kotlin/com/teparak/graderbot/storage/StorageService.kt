package com.teparak.graderbot.storage

import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.ListObjectsV2Result
import com.amazonaws.services.s3.model.ObjectMetadata
import java.io.File
import java.io.InputStream
import java.net.URL

interface StorageService {

    fun createBucket(bucket: String)
    fun removeBucket(bucket: String)
    fun bucketExist(bucket: String): Boolean

    fun isConnectionActive()

    fun putObject(bucket: String, objectName: String, inputStream: InputStream, metadata: ObjectMetadata)
    fun putObject(bucket: String, objectName: String, file: File)
    fun getObject(bucket: String, objectName: String): Pair<InputStream, ObjectMetadata>
    fun moveObject(srcBucket: String, srcObjectName: String, destBucket: String, destObjectName: String)
    fun removeObject(bucket: String, objectName: String)

    fun listObjects(bucket: String): ListObjectsV2Result
    fun listObjects(bucket: String, prefix: String): ListObjectsV2Result
    fun listBuckets(): List<Bucket>
    fun presignedDownloadURL(bucket: String, objectName: String, attachmentName: String? = null): URL
    fun presignedUploadURL(bucket: String, objectName: String): URL
}