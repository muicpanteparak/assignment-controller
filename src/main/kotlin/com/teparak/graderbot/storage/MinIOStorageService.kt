package com.teparak.graderbot.storage

import com.amazonaws.AmazonServiceException
import com.amazonaws.ClientConfiguration
import com.amazonaws.HttpMethod
import com.amazonaws.SdkClientException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.event.ProgressEvent
import com.amazonaws.event.ProgressEventType
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.*
import com.amazonaws.services.s3.transfer.TransferManager
import com.amazonaws.services.s3.transfer.TransferManagerBuilder
import com.amazonaws.services.s3.transfer.Upload
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.File
import java.io.InputStream
import java.net.URL
import java.time.Instant
import java.util.*

@Service
class MinIOStorageService// Check if connectable
@Autowired constructor(private val storageProperties: StorageServiceProperties) : StorageService {

    companion object {
        private val logger = LoggerFactory.getLogger(MinIOStorageService::class.java)
    }

    private val s3Client: AmazonS3
    private val tm: TransferManager

    init {
        val c = connect()
        s3Client = c.first
        tm = c.second
        this.isConnectionActive()
    }

    private fun connect(): Pair<AmazonS3, TransferManager> {
        val credentials = BasicAWSCredentials(storageProperties.accessKey, storageProperties.secretKey)

        val clientConfiguration = ClientConfiguration()
        clientConfiguration.signerOverride = "AWSS3V4SignerType"

        val s3Client: AmazonS3 = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration("${if (!storageProperties.secure) "http" else "https"}://${storageProperties.hostname}:${storageProperties.port}", Regions.US_EAST_1.name))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(AWSStaticCredentialsProvider(credentials))
                .build()!!

        val tm: TransferManager = TransferManagerBuilder.standard()
                .withS3Client(s3Client)
                .build()!!
        return Pair(s3Client, tm)
    }

    @Throws(AmazonServiceException::class, SdkClientException::class)
    override fun putObject(bucket: String, objectName: String, inputStream: InputStream, metadata: ObjectMetadata) {
        val upload = tm.upload(bucket, objectName, inputStream, metadata)!!
        registerCallback(upload, bucket, objectName)
    }

    override fun putObject(bucket: String, objectName: String, file: File) {
        val upload = tm.upload(bucket, objectName, file)!!
        registerCallback(upload, bucket, objectName)
    }

    override fun getObject(bucket: String, objectName: String): Pair<InputStream, ObjectMetadata> {
        val file = s3Client.getObject(bucket, objectName)!!
        return Pair(file.objectContent!!, file.objectMetadata!!)
    }

    private fun registerCallback(upload: Upload, bucket: String, objectName: String) {
        logger.debug("Registering Listener")
        upload.addProgressListener { listener: ProgressEvent ->
            when (listener.eventType) {
                ProgressEventType.TRANSFER_STARTED_EVENT -> logger.debug("TRANSFER STARTED '$bucket:$objectName'")
                ProgressEventType.TRANSFER_COMPLETED_EVENT -> logger.debug("TRANSFER COMPLETED '$bucket:$objectName'")
                ProgressEventType.TRANSFER_FAILED_EVENT -> logger.error("TRANSFER FAILED! '$bucket:$objectName'")
                else -> {
                }
            }
        }

        logger.debug("Waiting for FileObject to Complete")
        upload.waitForUploadResult()
    }

    override fun createBucket(bucket: String) {
        logger.debug("create bucket if not exist")
        when (this.bucketExist(bucket)){
            false -> {
                logger.debug("creating bucket...")
                s3Client.createBucket(bucket)
            }
        }
    }

    override fun removeBucket(bucket: String) {
        when (this.bucketExist(bucket)) {
            true -> {
                removeAllObjects(bucket)
                s3Client.deleteBucket(bucket)
            }
        }
    }

    override fun moveObject(srcBucket: String, srcObjectName: String, destBucket: String, destObjectName: String) {
        logger.debug("Copying '$srcBucket:$srcObjectName' to '$destBucket:$destObjectName'")
        createBucket(destBucket)
        try {
            s3Client.copyObject(srcBucket, srcObjectName, destBucket, destObjectName)
            s3Client.deleteObject(srcBucket, srcObjectName)
        } catch (e: SdkClientException) {
            logger.warn(e.message)
        }
    }

    override fun bucketExist(bucket: String): Boolean {
        return s3Client.doesBucketExistV2(bucket)
    }

    override fun listObjects(bucket: String): ListObjectsV2Result {
        return listObjects(bucket, "/")
    }

    override fun listObjects(bucket: String, prefix: String): ListObjectsV2Result {
        return s3Client.listObjectsV2(bucket, prefix)
    }

    override fun listBuckets(): List<Bucket> {
        return s3Client.listBuckets()
    }

    override fun removeObject(bucket: String, objectName: String) {
        s3Client.deleteObject(bucket, objectName)
    }

    private fun removeAllObjects(bucket: String) {
        logger.debug("removing all objects")
        do {
            val pointer = listObjects(bucket)
            pointer.objectSummaries.forEach {
                removeObject(it.bucketName, it.key)
            }
        } while (pointer.isTruncated)
    }

    override fun presignedDownloadURL(bucket: String, objectName: String, attachmentName: String?): URL {
        val name = attachmentName ?: objectName

        logger.debug("generate presign for $bucket:$objectName with attachment name $name")


        val expTime = Date.from(Instant.now().plusSeconds(60 * 60))

        val responseOverride = ResponseHeaderOverrides().apply {
            // TODO(Potential Bug)
            this.contentDisposition = "attachment; filename=\"$name\""
        }
        val presignedRequest = GeneratePresignedUrlRequest(bucket, objectName)
                .withMethod(HttpMethod.GET)
                .withExpiration(expTime)
                .withResponseHeaders(responseOverride)


        return s3Client.generatePresignedUrl(presignedRequest)!!
    }

    override fun presignedUploadURL(bucket: String, objectName: String): URL {
        val expTime = Date.from(Instant.now().plusSeconds(60 * 60))


        val presignedRequest = GeneratePresignedUrlRequest(bucket, objectName)
                .withMethod(HttpMethod.PUT)
                .withExpiration(expTime)

        return s3Client.generatePresignedUrl(presignedRequest)!!
    }

    @Throws(Exception::class)
    override fun isConnectionActive() {
        try {
            logger.debug("Checking minio connection state...")
            s3Client.listBuckets()
            logger.debug("Connection active")
        } catch (e: Exception) {
            logger.error("Connection inactive")
            logger.error(e.message)
            throw Exception(e.message)
        }
    }
}