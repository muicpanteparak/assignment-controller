package com.teparak.graderbot.exception

class BadRequestException(message: String? = null) : RuntimeException(message)