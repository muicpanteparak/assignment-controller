package com.teparak.graderbot.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class RecordNotFoundException(message: String?) : RuntimeException(message, null, false, false)