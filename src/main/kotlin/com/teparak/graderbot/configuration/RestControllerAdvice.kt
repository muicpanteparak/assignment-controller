package com.teparak.graderbot.configuration

import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.time.Instant
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestControllerAdvice
class RestControllerAdvice {

    @ExceptionHandler(HttpMessageNotReadableException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleRecordNotFound(ex: Throwable, request: HttpServletRequest, response: HttpServletResponse): ErrorResponse {

        val code = HttpStatus.BAD_REQUEST
        return ErrorResponse(code.reasonPhrase, ex.message ?: "", request.requestURI, code.value())
    }
}

data class ErrorResponse(val error: String, val message: String, val path: String, val status: Int, val timestamp: Instant = Instant.now())