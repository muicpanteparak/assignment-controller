package com.teparak.graderbot.configuration

import org.modelmapper.ModelMapper
import org.modelmapper.convention.NameTokenizers
import org.modelmapper.jackson.JsonNodeValueReader
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ModelMapperConfiguration {
    companion object {
        var model: ModelMapper? = null
    }

    @Bean
    fun config(): ModelMapper {
        val mapper = ModelMapper()
        mapper.configuration.addValueReader(JsonNodeValueReader())
        mapper.configuration.sourceNameTokenizer = NameTokenizers.UNDERSCORE
        model = mapper
        return mapper
    }
}